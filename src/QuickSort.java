import com.sun.xml.internal.bind.v2.model.annotation.Quick;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * Created by Anna Bonaldo on 31/08/2017.
 */
public class QuickSort {
    public static int[] data = null;
    public static int cutOff = 0;

    public static void sortArray (final Scheduler sched, final int[] arrayToSort, final int seqCutoff) {

        data = arrayToSort;
        cutOff = seqCutoff;

        HashSet<Tasklet> master = new HashSet<Tasklet>();
        QSortTasklet t = new QSortTasklet(master, 0, arrayToSort.length-1);
        sched.spawn(t);
        sched.waitForAll(master);

    }  //  end sortArray

    public static  int partition(int[] array, int left, int right)
    {
        int pivot = array[left];
        while (left <= right)
        {
            while (array[left] < pivot)
            {left++;}

            while (array[right] > pivot)
            {right--;}

            if (left <= right)
            {
                int tmp = array[left];
                array[left] = array[right];
                array[right] = tmp;

                left++;
                right--;
            }
        }
        return left;
    }

    public static  void sequentialSortArray(int arr[], int left, int right) {
        int index = partition(arr, left, right);
        if (left < index - 1)
            sequentialSortArray(arr, left, index - 1);
        if (index < right)
            sequentialSortArray(arr, index, right);
    }

    public static void main(String[] args)
    {
        List<String[]> input = IOFromCSVFile.readInput();
        List<WorkStealingScheduler.Statistics> statistics = new ArrayList<WorkStealingScheduler.Statistics>();
        for (String[] in_args : input) {
            statistics.add(quicksort(in_args));
        }
        IOFromCSVFile.writeOutput();

    }

    public static WorkStealingScheduler.Statistics quicksort(String[] args){
        WorkStealingScheduler.Statistics statistics = new WorkStealingScheduler.Statistics();
        if (args.length != 3) {
            System.out.println("Usage: pqsort arr_size num_servers seq_cutoff");
            return;
        }
        final int arrSize = Integer.parseInt(args[0]);
        final int numServers = Integer.parseInt(args[1]);
        final int seqCutoff = Integer.parseInt(args[2]);

        int[] arrayToSort = new int[arrSize];
        Random ran = new Random((long)arrSize);

        for (int i = 0; i < arrSize; i++) {
            arrayToSort[i] = ran.nextInt(arrSize*2);
        }
        if (arrSize <= 100) {
            for (int i = 0; i < arrSize; i++) {
                System.out.print(Integer.toString(arrayToSort[i]) + " ");
            }
            System.out.println("");
            System.out.println("");
        }
        boolean sortConcurrent = true;
        boolean sortSequential = true;
        int[] sequentialCopy = arrayToSort.clone();
        if(sortConcurrent) {

            System.out.println("CONCURRENT SORTING");
            WorkStealingScheduler sched = new WorkStealingScheduler(numServers);
            long startTimeA = System.currentTimeMillis();
            long startTimeB = System.nanoTime();
            sortArray(sched, arrayToSort, seqCutoff);
            sched.shutdown();
            long endTimeA = System.currentTimeMillis();
            long endTimeB = System.nanoTime();

            sched.printStats();
            System.out.println("Total time millis: " + (endTimeA - startTimeA) );
            System.out.println("Total nano-time: " + (endTimeB - startTimeB) );
        }

        if(sortSequential)
        {
            System.out.println("SEQUENTIAL SORTING");
            WorkStealingScheduler sched = new WorkStealingScheduler(0);
            long startTimeA = System.currentTimeMillis();
            long startTimeB = System.nanoTime();
            sched.executeSequential(sequentialCopy);
            long endTimeA = System.currentTimeMillis();
            long endTimeB = System.nanoTime();
            System.out.println("Total time millis: " + (endTimeA - startTimeA) );
            System.out.println("Total nano-time: " + (endTimeB - startTimeB) );
        }

        if (arrSize <= 100) {
            System.out.println("");
            for (int i = 0; i < arrSize; i++)
                System.out.print(Integer.toString(arrayToSort[i]) + " ");

            System.out.println('\n');

            for (int i = 0; i < arrSize; i++)
                System.out.print(Integer.toString(sequentialCopy[i]) + " ");
        }
    }
} //  end class QuickSort

class QSortTasklet extends Tasklet {

    public final int start, end;
    private QSortTasklet[] children = null;


    public QSortTasklet(HashSet<Tasklet> master, int start, int end) {
        super(master);
        super.master.add(this);
        this.start = start;
        this.end = end;
    }

    public void EndTasklet()
    {
        this.master.remove(this);
    }

    @Override
    public Tasklet[] getChildren() {
        return children;
    }

    public void run() {
        long tStart = System.nanoTime();
        int[] array = QuickSort.data;

        if ((end - start + 1) > QuickSort.cutOff)
        {
            doConcurrentStuff(array);

        }
        else
        {
            QuickSort.sequentialSortArray(array, start, end);
        }

        EndTasklet();

    }

    @Override
    public boolean isLeaf() {
        return (children == null);// || (children[0] == null) || (children[1] == null);
    }

    @Override
    public boolean isChildOf(Tasklet t) {
        /*QSortTasklet parent = (QSortTasklet) t;
        return  (parent != null) && ((parent.start <= this.start) && (this.end <= parent.end));*/
        return true;
    }


    public void doConcurrentStuff(int[] array)
    {
        if (start < end) {
            //System.out.println("start < end");
            int split_position = QuickSort.partition(array, start, end);

            children = new QSortTasklet[2];

            if (start < split_position - 1) {
                int start_a = start;
                int end_a = split_position - 1;
                //System.out.println("sto per creare figlio con " + start_a + end_a);
                QSortTasklet a = new QSortTasklet(master, start_a, end_a);
                children[0] = a;

                //System.out.println("CREATO UN FIGLIO CON START E END " + start_a + end_a);
            }

           if (end > split_position) {
                int start_b = split_position;
                int end_b = end;
                QSortTasklet b = new QSortTasklet(master, start_b, end_b);
                children[1] = b;
                //System.out.println("CREATO UN FIGLIO CON START E END " + start_b + end_b);
            }

        }
    }
} // end class QSortTasklet