import java.util.HashSet;
import java.util.Random;
import java.util.Stack;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Anna Bonaldo on 31/08/2017.
 */
public interface Scheduler {

    public void spawn(Tasklet t);
    //  Add tasklet to t.master and to queue of server t.serverIndex
    public void waitForAll(HashSet<Tasklet> master);
    //  Wait for all
    public void printStats();
}

class WorkStealingScheduler implements Scheduler {

    private static AtomicBoolean shutdownNow = new AtomicBoolean(false);

    private static ThreadLocal<Integer> serverIndex = new ThreadLocal<Integer>();

    final  private  ServerThread[] servers;


    private class ServerThread implements Runnable {
        private int myIndex = 0;
        public ConcurrentLinkedDeque<Tasklet> deque = new ConcurrentLinkedDeque<>();
        private Stack<Tasklet> stack = new Stack<>();
        public  final Statistics stats = new Statistics();


        public void run() {

            long tStart = System.nanoTime();
            long millisStart= System.currentTimeMillis();
            serverIndex.set(myIndex);
            //boolean keepWorking = true; //!WorkStealingScheduler.shutdownNow.get();
            //System.out.println(myIndex);

            while (!shutdownNow.get()) //CAMBIATA LA CONDIZIONE
            {
                //FARE IN MODO CHE CONSIDERI TUTTI I SERVER
                if(deque.isEmpty())
                    steal();

                while (!deque.isEmpty()) {
                    Tasklet t = deque.pollLast();

                    if (t != null)
                    {
                        stack.push(t);

                        Thread t1 = new Thread(t);
                        servers[myIndex].stats.numTaskletInitiations++;


                        t1.start();

                        try {
                            t1.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (!t.isLeaf()) {
                            Tasklet[] children = t.getChildren();

                            if (children[0] != null) {
                                deque.addLast(children[0]);
                                //System.out.println("DEQUE di " + myIndex + ": " + deque.size());
                            }
                            if (children[1] != null) {
                                deque.addLast(children[1]);
                                //System.out.println("DEQUE di " + myIndex + ": " + deque.size());
                            }
                        }

                        stack.pop();
                    }
                }
                shutdownNow.set(checkServers());
            }
            stats.CPUtime = System.currentTimeMillis() - millisStart;
            stats.wallClockTime = System.nanoTime() - tStart;
        } // end run

        private boolean checkServers()
        {
            for (int i=0; i<servers.length; i++ ) {
                if (!servers[i].deque.isEmpty()||!servers[i].stack.isEmpty())
                    return false;
            }
            return true;
        }


        public boolean steal()
        {
            //System.out.println("Sono nel metodo steal");
            boolean stolen = false;

            for (int i=0; i<servers.length && (!stolen); i++ ) {
                //System.out.println("Sono nel FOR  metodo steal");
                if (i != myIndex && !servers[i].deque.isEmpty()) {
                    Tasklet t = servers[i].deque.pollFirst();
                    if (t != null) {
                        // stack.push(t);
                        // System.out.println("DEQUE da cui rubo: "+i+" di dimensione "+servers[i].deque.size());
                        servers[myIndex].deque.addLast(t);
                        //System.out.println("***STEAL*** DEQUE di "+myIndex+" "+deque.size());
                        servers[myIndex].stats.numTaskletSteals++;
                        //System.out.println("rubato da server "+ i + " a server " + myIndex);
                        stolen = true;
                    }

                }
            }
       /*    for (int i=0; i<iniz && (!stolen); i++ )
            {
                //System.out.println("Sono nel FOR  metodo steal");
                if(i!=myIndex && !servers[i].deque.isEmpty())
                {
                    Tasklet t = servers[i].deque.pollFirst();
                    if (t!=null)
                    {
                        // stack.push(t);
                        // System.out.println("DEQUE da cui rubo: "+i+" di dimensione "+servers[i].deque.size());
                        servers[myIndex].deque.addLast(t);
                        //System.out.println("***STEAL*** DEQUE di "+myIndex+" "+deque.size());
                        servers[myIndex].stats.numTaskletSteals++;
                        //System.out.println("rubato da server "+ i + " a server " + myIndex);
                        stolen = true;
                    }
                }

            }*/
            return stolen;

        }


        public ServerThread(int myIndex) {
            this.myIndex = myIndex;
        }

    } // end class ServerThread

    public WorkStealingScheduler(int numServers) {
        //  Create the array of servers, and then start them all running
        long start = System.nanoTime();
        this.servers = new ServerThread[numServers];
        for (int i = 0; i < numServers; i++) {
            servers[i] = new ServerThread(i);
        }
        long end = System.nanoTime();
        System.out.println("Servers creation : "+(end-start));

    }

    public void spawn(Tasklet t) {
        if (servers[0] != null)
            servers[0].deque.addLast(t);
    }

    public void waitForAll(HashSet<Tasklet> master) {
        if (master.isEmpty())
        {
            this.shutdown();
        }

        else {
            Thread[] prova=new Thread[servers.length];
            try
            {
                long start = System.nanoTime();
                for (int i = 0; i < servers.length; i++) {
                    prova[i] = new Thread(servers[i]);
                    prova[i].start();
                }
                long end = System.nanoTime();
                System.out.println("Servers starting : "+(end-start));

                for (int i = 0; i < servers.length; i++) {
                    prova[i].join();
                }
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void shutdown() {
        shutdownNow.set(true);
    }

    public void executeSequential(int[] arrayToSort) {
        QuickSort.sequentialSortArray(arrayToSort, 0, arrayToSort.length-1);
    }

    public void printStats() {
        System.out.println("server\tsteals\tinits");
        int totalSteals = 0;
        int totalInitiations = 0;
        for (int i = 0; i < servers.length; i++) {
            int numSteals = servers[i].stats.concStats.numTaskletSteals;
            int numInitiations = servers[i].stats.numTaskletInitiations;

            totalSteals += numSteals;
            totalInitiations += numInitiations;
            System.out.println(Integer.toString(i) + "\t" +
                    numSteals + "\t" + numInitiations);
        }
        System.out.println("total\t" + totalSteals + "\t" + totalInitiations);

        System.out.println("-----------------------------------------------");
        System.out.println("-----------------------------------------------");
        long totalCPU =0;
        long totalWallclock =0;
        for (int i = 0; i < servers.length; i++) {
             totalCPU += servers[i].stats.concStats.totalCPUTime;
             totalWallclock += servers[i].stats.concStats.totalClockTime;
            System.out.println("Server "+Integer.toString(i) + "CPU time = " + servers[i].stats.concStats.totalCPUTime);
            System.out.println("Server "+Integer.toString(i) + "WALL CLOCK time = " + servers[i].stats.concStats.totalClockTime);
            System.out.println("------------");

        }

        System.out.println("TOTAL CPU time = " +totalCPU);
        System.out.println("TOTAL WALL CLOCK time = " + totalWallclock);

        System.out.println("-----------------------------------------------");
        System.out.println("-----------------------------------------------");

    }
} // end class WorkStealingScheduler
