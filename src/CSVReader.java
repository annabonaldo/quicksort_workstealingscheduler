import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Anna Bonaldo on 08/09/2017.
 */


public class CSVReader {

    private static final char DEFAULT_SEPARATOR = ';';
    private static final char DEFAULT_QUOTE = '"';
    static String inputFile = "C:\\Users\\Anna Bonaldo\\Documents\\Code\\new\\src\\inputData.csv";
    static String outputFile = "C:\\Users\\Anna Bonaldo\\Documents\\Code\\new\\src\\outputData.csv";


    public static List<String[]> readInput(String[] args)
    {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(inputFile));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        ArrayList<String[]> allInput = new ArrayList<String[]>();

        if (scanner != null) {
            while (scanner.hasNext()) {
            List<String> line = parseLine(scanner.nextLine());
            String[] input = new String[3];
            input[0] = line.get(0);
            System.out.println(input[0]);
            input[1] = line.get(1);
            System.out.println(input[1]);
            input[2] = line.get(2);
            System.out.println(input[2]);
            allInput.add(input);
        }
        }
        scanner.close();
        return  allInput;
    }

    public static void writeOutput(List<WorkStealingScheduler.Statistics> statList)
    {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new File(outputFile));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("id");
        sb.append(',');
        sb.append("Name");
        sb.append('\n');

        sb.append("1");
        sb.append(',');
        sb.append("Prashant Ghimire");
        sb.append('\n');

        pw.write(sb.toString());
        pw.close();
        System.out.println("done!");
    }


    public static List<String> parseLine(String cvsLine) {
        return parseLine(cvsLine, DEFAULT_SEPARATOR, DEFAULT_QUOTE);
    }

    public static List<String> parseLine(String cvsLine, char separators) {
        return parseLine(cvsLine, separators, DEFAULT_QUOTE);
    }

    public static List<String> parseLine(String cvsLine, char separators, char customQuote) {

        List<String> result = new ArrayList<>();

        //if empty, return!
        if (cvsLine == null && cvsLine.isEmpty()) {
            return result;
        }

        if (customQuote == ' ') {
            customQuote = DEFAULT_QUOTE;
        }

        if (separators == ' ') {
            separators = DEFAULT_SEPARATOR;
        }

        StringBuffer curVal = new StringBuffer();
        boolean inQuotes = false;
        boolean startCollectChar = false;
        boolean doubleQuotesInColumn = false;

        char[] chars = cvsLine.toCharArray();

        for (char ch : chars) {

            if (inQuotes) {
                startCollectChar = true;
                if (ch == customQuote) {
                    inQuotes = false;
                    doubleQuotesInColumn = false;
                } else {

                    //Fixed : allow "" in custom quote enclosed
                    if (ch == '\"') {
                        if (!doubleQuotesInColumn) {
                            curVal.append(ch);
                            doubleQuotesInColumn = true;
                        }
                    } else {
                        curVal.append(ch);
                    }

                }
            } else {
                if (ch == customQuote) {

                    inQuotes = true;

                    //Fixed : allow "" in empty quote enclosed
                    if (chars[0] != '"' && customQuote == '\"') {
                        curVal.append('"');
                    }

                    //double quotes in column will hit this!
                    if (startCollectChar) {
                        curVal.append('"');
                    }

                } else if (ch == separators) {

                    result.add(curVal.toString());

                    curVal = new StringBuffer();
                    startCollectChar = false;

                } else if (ch == '\r') {
                    //ignore LF characters
                    continue;
                } else if (ch == '\n') {
                    //the end, break!
                    break;
                } else {
                    curVal.append(ch);
                }
            }

        }

        result.add(curVal.toString());

        return result;
    }

}
